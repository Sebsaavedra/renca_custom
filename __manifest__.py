
{
    'name': 'New models implementation',
    'version': '11.0.1.0.0',
    'category': 'Undefined',
    'author': '[Amaris]',
    'website': 'http://www.amaris.com',
    'description': """This module add support for fields and actions launched from Renca System""",
    'depends':['account_cost_center','account','sale', 'account_budget'],
    'data': [
        'security/ir.model.access.csv',
        'views/assignment_accouting_account_view.xml',
        'views/item_accouting_account_view.xml',
        'views/subtitle_accouting_account_view.xml',
        'views/account_account_view.xml',
        'views/service_type_view.xml',
        'views/service_view.xml',
        'views/account_cost_center_area_view.xml',        
        'views/account_invoice_view.xml',
        'views/account_budget_view.xml',
        'views/account_move_view.xml',
        'views/account_cost_center_view.xml',
        'views/purchase_order_type_view.xml',
        'views/menu_root.xml',

    ],
    'contributors': [
        'Luis Eduardo campos <luis.camposcerda@amaris.com>'
    ]
}