from odoo import fields, models

class AccountCostCenter(models.Model):
    _inherit = 'account.cost.center'

    area_id = fields.Many2one('account.cost.center.area' ,string='Area', required=True)
    