from odoo import api, models, fields
import logging

_logger = logging.getLogger(__name__)


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    purchase_order_type_id = fields.Many2one('purchase.order.type')


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'

    service_id = fields.Many2one('service', string='Service or project', readonly=True,
                                 states={'draft': [('readonly', False)]}, help='Default Service or Proyect.')
    cost_center_id = fields.Many2one('account.cost.center', string='Cost Center', help='Default Cost Center')
    account_id = fields.Many2one(related='product_id.property_account_expense_id',help="The income or expense account related to the selected product.")


class PurchaseOrderType(models.Model):
    _name = 'purchase.order.type'
    _description = ' Purchase order type'

    name = fields.Char('Purchase order type', required=True)
