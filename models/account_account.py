from odoo import api, fields, models


class AccountAccount(models.Model):
    _inherit = 'account.account'

    assignment_id = fields.Many2one('assignment.accounting.account', string='Assignment', required=True)
    item_id = fields.Many2one(string="Item", related='assignment_id.item_id')
    subtitle_id = fields.Many2one(string="Subtitle", related='item_id.subtitle_id')

    #Sh!7#m%Txv4D