import logging
from odoo import api, fields, models

_logger = logging.getLogger(__name__)

class AssignmentAccoutingAccount(models.Model):
    _name = 'assignment.accounting.account'
    _description = 'Assignment accounting account'

    name = fields.Char(string='Assignment', required=True)
    code = fields.Char(string='Code', required=True)
    item_id = fields.Many2one('item.accounting.account', string='Item', required=True)
    subtitle_id = fields.Many2one(related='item_id.subtitle_id', string='Subtitle', readonly=True)

    def name_get(self):
        new_name = []
        for assignment in self:
            name = "%s - (%s)" % (assignment.code, assignment.name)
            # name = "%s - %s (%s - %s) - (%s - %s)" % (assignment.code, assignment.name, assignment.item_id.code, assignment.item_id.name, assignment.item_id.subtitle_id.code, assignment.item_id.subtitle_id.name
            new_name.append((assignment.id, name))
        return new_name