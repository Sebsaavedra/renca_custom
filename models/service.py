from odoo import api, fields, models


class Service(models.Model):
    _name = 'service'
    _description = 'Service or Project'

    name = fields.Char(string='Service or Project Name', required=True)
    code = fields.Char(required=True)
    type_id = fields.Many2one('service.type', string='Type of Service or Project', required=True)
