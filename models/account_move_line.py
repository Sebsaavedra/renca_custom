from odoo import fields, models


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    service_id = fields.Many2one(
        'service', string='Service or project', 
        index=True, 
        help="Type of service or project.")
    type_id = fields.Many2one(related='service_id.type_id')
    area_id = fields.Many2one(related='cost_center_id.area_id')
