from odoo import api, fields, models


class ServiceType(models.Model):
    _name = 'service.type'
    _description = 'Types of Service or project'

    name = fields.Char(string='Name of the project or service type ', required=True)
