from odoo import api, fields, models


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'


    @api.model
    def _default_service(self):
        return self.env['service'].browse(
            self.env.context.get('service_id'))


    service_id = fields.Many2one(
        'service',
        string='Service or project',
        index=True,
        default=lambda self: self._default_service(),
    )