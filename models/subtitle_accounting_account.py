from odoo import api, fields, models, _

class SubtitleAccoutingAccount(models.Model):
    _name = 'subtitle.accounting.account'
    _description = 'Subtitle accouting account'

    name = fields.Char(string='Subtitle', required=True)
    code = fields.Char(string='Code', required=True)

    def name_get(self):
        new_name = []
        for subtitle in self:
            name = "%s - (%s)" % (subtitle.code, subtitle.name)
            new_name.append((subtitle.id, name))
        return new_name
  