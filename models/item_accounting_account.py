import logging
from odoo import api, fields, models

_logger = logging.getLogger(__name__)

class ItemAccoutingAccount(models.Model):
    _name = 'item.accounting.account'
    _description = 'Item accouting account'

    name = fields.Char(string='Items', required=True)
    code = fields.Char(string='Code', required=True)
    subtitle_id = fields.Many2one('subtitle.accounting.account', string='Subtitle', required=True)
    

    def name_get(self):
        new_name = []
        for item in self:
            name = "%s - (%s)" % (item.code, item.name)
            new_name.append((item.id, name))
        return new_name

