
from odoo import api, fields, models


class AccountCostCenterArea(models.Model):
    _name = 'account.cost.center.area'
    _description = 'Module description'

    name = fields.Char(string="Name", required=True)
    description = fields.Char(string="Description" )
