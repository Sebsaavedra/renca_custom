from lxml import etree
from odoo import api, models, fields
import logging

_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    service_id = fields.Many2one('service', string="Service or project", readonly=True, 
                                    states={'draft': [('readonly', False)]}, help="Explain your field.")

    def _prepare_invoice_line_from_po_line(self, line):
        res = super(AccountInvoice, self)._prepare_invoice_line_from_po_line(line)
        res['service_id'] = line.service_id.id
        res['cost_center_id'] = line.cost_center_id.id
        return res

    @api.model
    def line_get_convert(self, line, part):
        res = super(AccountInvoice, self).line_get_convert(line, part)
        if line.get('service_id'):
            res['service_id'] = line['service_id']
        return res

    @api.model
    def fields_view_get(self, view_id=None, view_type='form',
                        toolbar=False, submenu=False):
        res = super(AccountInvoice, self).fields_view_get(
            view_id=view_id, view_type=view_type,
            toolbar=toolbar, submenu=submenu)

        if not self._context.get('service_default', False):
            if view_type == 'form':
                view_obj = etree.XML(res['arch'])
                invoice_line = view_obj.xpath(
                    "//field[@name='invoice_line_ids']"
                )
                extra_ctx = "'service_default': 1, " \
                    "'service_id': service_id"
                for el in invoice_line:
                    ctx = "{" + extra_ctx + "}"
                    if el.get('context'):
                        ctx = el.get('context')
                        ctx_strip = ctx.rstrip("}").strip().rstrip(",")
                        ctx = ctx_strip + ", " + extra_ctx + "}"

                    el.set('context', str(ctx))
                    res['arch'] = etree.tostring(view_obj)
        return res

    @api.model
    def invoice_line_move_line_get(self):
        res = super(AccountInvoice, self).invoice_line_move_line_get()
        for dict_data in res:
            invl_id = dict_data.get('invl_id')
            line = self.env['account.invoice.line'].browse(invl_id)
            if line.service_id:
                dict_data['service_id'] = line.service_id.id
        return res
