
import logging
from datetime import datetime
from odoo import api, fields, models, _

_logger = logging.getLogger(__name__)

class CrossoverdBudget(models.Model):
    _inherit = 'crossovered.budget'

    date_from = fields.Date('Start Date', required=True, states={'done': [('readonly', True)]}, default=datetime.today())
    date_to = fields.Date('End Date', required=True, states={'done': [('readonly', True)]}, default=datetime.today())

    @api.multi
    def action_recalculate_lines(self):
        for line in self.crossovered_budget_line:
            line._compute_get_fields()
            line._compute_theoritical_amount()
            line._compute_percentage()

class CrossoverdBudgetLine(models.Model):
    _inherit = 'crossovered.budget.lines'

    analytic_account_id = fields.Many2one('account.analytic.account', 'Analytic Account')
    general_budget_id = fields.Many2one('account.budget.post', 'Budgetary Position', required=False)
    account_id = fields.Many2one('account.account', string='Account', index=True, ondelete="cascade", domain=[('deprecated', '=', False)])
    cost_center_id = fields.Many2one('account.cost.center', index=True, string='Cost Center')
    service_id = fields.Many2one('service', index=True, String='Service')
    area_id = fields.Many2one('account.cost.center.area', string='Area')
    assignment_id = fields.Many2one('assignment.accounting.account', string='Assignment')
    item_id = fields.Many2one('item.accounting.account', string="Item")
    subtitle_id = fields.Many2one('subtitle.accounting.account', string="Subtitle")    
    type_id = fields.Many2one('service.type', string='Type of Service or Project')
    practical_amount_alt = fields.Float('Practical amount', compute='_compute_get_fields', store=True)
    theoritical_amount = fields.Float(compute='_compute_theoritical_amount', string='Theoretical Amount',digits=0, store=True)
    percentage = fields.Float(compute='_compute_percentage', string='Achievement', store=True)
    executed_amount = fields.Float('Executed amount', compute="_compute_get_fields", store=True)
    committed_amount = fields.Float('Committed amount', compute="_compute_get_fields", store=True)
    total_amount = fields.Float('Total import', compute="_compute_get_fields", store=True)
    available_amount = fields.Float('Available amount', compute="_compute_get_fields", store=True)

    account_filter_selection = fields.Selection([
        ('none', 'no account'),
        ('account', 'account'),
        ('account_assig', 'assignment'),
        ('assig_item', 'item'),
        ('item_subt', 'subtitle')], default='none')
    cost_center_filter_selection = fields.Selection([
        ('none', 'no cost center'),
        ('cost_center', 'cost center'),
        ('cost_area', 'area')], default='none')
    service_filter_selection = fields.Selection([
        ('none', 'no service'),
        ('service', 'service'),
        ('service_type', 'service type')], default='none')


    @api.onchange('account_filter_selection', 'cost_center_filter_selection', 'service_filter_selection')
    def unlink_filter(self):
        data = {}
        if self.account_filter_selection == 'none':
            data['account_id'] = False
            data['assignment_id'] = False
            data['item_id'] = False
            data['subtitle_id'] = False


        if self.service_filter_selection == 'none':
            data['service_id'] = False
            data['type_id'] = False
           
        if self.cost_center_filter_selection == 'none':
            data['cost_center_id'] = False
            data['area_id'] = False            
        self.update(data)       


    @api.depends('account_id', 'service_id', 'cost_center_id', 'area_id', 'assignment_id', 'item_id', 'subtitle_id', 'type_id', 'analytic_account_id')
    def _compute_get_fields(self):            
        for record in self:
            data = {'planned_amount': record.planned_amount,'practical_amount_alt':0, 'committed_amount':0, 'executed_amount':0, 'total_amount':0, 'available_amount':0}
            move_line_object = self.env['account.move.line']
            tipos = self.env["account.account.type"].search([("name", "in", ['Activos Corrientes', 'Activos fijos', 'Ingreso', 'Otro Ingreso', 'Gastos'])])
            move_line_filters = []
            invoice_line_filter = []
            oc_line_filter = []

            ## cuenta analitica
            if record.analytic_account_id:
                move_line_filters.append(('analytic_account_id', '=', record.analytic_account_id.id))
                invoice_line_filter.append(('account_analytic_id', '=', record.analytic_account_id.id))
                oc_line_filter.append(('account_analytic_id', '=', record.analytic_account_id.id))

            ## agrupacion de campos relacionados por cuenta
            if record.account_filter_selection == 'account' and record.account_id:
                move_line_filters.append(('account_id', '=', record.account_id.id))
                invoice_line_filter.append(('account_id', '=', record.account_id.id))
                oc_line_filter.append(('account_id', '=', record.account_id.id))
                data['assignment_id'] = record.account_id.assignment_id.id
                data['item_id'] = record.account_id.assignment_id.item_id.id
                data['subtitle_id'] = record.account_id.assignment_id.item_id.subtitle_id.id

            if record.account_filter_selection == 'account_assig' and record.assignment_id:
                move_line_filters.append(('account_id.assignment_id', '=', record.assignment_id.id))
                invoice_line_filter.append(('account_id.assignment_id', '=', record.assignment_id.id))
                oc_line_filter.append(('account_id.assignment_id', '=', record.assignment_id.id))
                data['item_id'] = record.assignment_id.item_id.id
                data['subtitle_id'] = record.assignment_id.item_id.subtitle_id.id

            if record.account_filter_selection == 'assig_item' and record.item_id:
                move_line_filters.append(('account_id.assignment_id.item_id', '=', record.item_id.id))
                invoice_line_filter.append(('account_id.assignment_id.item_id', '=', record.item_id.id))
                oc_line_filter.append(('account_id.assignment_id.item_id', '=', record.item_id.id))
                data['subtitle_id'] = record.item_id.subtitle_id.id

            if record.account_filter_selection == 'item_subt' and record.subtitle_id:
                move_line_filters.append(('account_id.assignment_id.item_id.subtitle_id', '=', record.subtitle_id.id))
                invoice_line_filter.append(('account_id.assignment_id.item_id.subtitle_id', '=', record.subtitle_id.id))
                oc_line_filter.append(('account_id.assignment_id.item_id.subtitle_id', '=', record.subtitle_id.id))

            ## agrupacion de campos relacionados por servicio
            if record.service_filter_selection == 'service' and record.service_id:
                move_line_filters.append(('service_id', '=', record.service_id.id))
                invoice_line_filter.append(('service_id', '=', record.service_id.id))
                oc_line_filter.append(('service_id', '=', record.service_id.id))
                data['type_id'] = record.service_id.type_id.id
                
            if record.service_filter_selection == 'service_type' and record.type_id:
                move_line_filters.append(('service_id.type_id', '=', record.type_id.id))
                invoice_line_filter.append(('service_id.type_id', '=', record.type_id.id))
                oc_line_filter.append(('service_id.type_id', '=', record.type_id.id))

            ## agrupacion de campos relacionados por centro de costo
            if record.cost_center_filter_selection == 'cost_center' and record.cost_center_id:
                move_line_filters.append(('cost_center_id', '=', record.cost_center_id.id))
                invoice_line_filter.append(('cost_center_id', '=', record.cost_center_id.id))
                oc_line_filter.append(('cost_center_id', '=', record.cost_center_id.id))
                data['area_id'] = record.cost_center_id.area_id.id

            if record.cost_center_filter_selection == 'cost_area' and record.area_id:
                move_line_filters.append(('cost_center_id.area_id', '=', record.area_id.id))
                invoice_line_filter.append(('cost_center_id.area_id', '=', record.area_id.id))
                oc_line_filter.append(('cost_center_id.area_id', '=', record.area_id.id))

            if len(move_line_filters) > 0:
                if (record.date_from != False) & (record.date_to != False):
                    move_line_filters += [("date",">=",record.date_from),("date","<=",record.date_to),("move_id.state", "=", "posted"),("user_type_id","in",tipos.ids)]

                #busqueda apuntes
                move_lines = move_line_object.search_read(move_line_filters, fields=['balance'])
                data['executed_amount'] = -sum([l["balance"] for l in move_lines])

                #busqueda lineas factura
                """invoice_line_filter += [("invoice_id.type", "=", "in_invoice"), ("invoice_id.state", "=", "open"), ("invoice_id.date_invoice",">=",record.date_from),("invoice_id.date_invoice","<=",record.date_to), ("account_id.user_type_id","in",tipos.ids)]
                inv_lines = self.env["account.invoice.line"].search_read(invoice_line_filter, fields=['purchase_line_id', 'price_total'])
                invs_total = sum([i['price_total'] for i in inv_lines])"""

                #busqueda ordenes de compra
                oc_line_filter += [('date_planned', '>=', record.date_from), ("date_planned", "<=", record.date_to), ("account_id.user_type_id","in",tipos.ids)]
                oc_lines = self.env['purchase.order.line'].search_read(oc_line_filter, fields=['price_total', 'price_unit', 'product_qty', 'qty_invoiced'])
                data['committed_amount'] = -sum([(i['product_qty'] - i['qty_invoiced']) * i['price_unit'] for i in oc_lines if i['qty_invoiced'] < i['product_qty'] or i['qty_invoiced'] == 0])
                data['total_amount'] = data['executed_amount'] + data['committed_amount']
                data['available_amount'] = data['planned_amount'] - data['total_amount']
                if (data['planned_amount'] < 0) & (data['planned_amount'] > data['total_amount']):
                    data['available_amount'] = 0
                if (data['planned_amount'] > 0) & (data['planned_amount'] < data['total_amount']):
                    data['available_amount'] = 0
            record.update(data)


    @api.multi
    @api.depends('theoritical_amount')
    def _compute_percentage(self):
        for line in self:
            if line.theoritical_amount != 0.00:
                line.percentage = float((line.executed_amount or line.executed_amount or 0.0) / line.theoritical_amount) * 100
            else:
                line.percentage = 0.00

    @api.multi
    @api.depends('planned_amount')
    def _compute_theoritical_amount(self):
        today = fields.Date.today()
        for line in self:
            # Used for the report
            if self.env.context.get('wizard_date_from') and self.env.context.get('wizard_date_to'):
                date_from = fields.Datetime.from_string(self.env.context.get('wizard_date_from'))
                date_to = fields.Datetime.from_string(self.env.context.get('wizard_date_to'))
                if date_from < fields.Datetime.from_string(line.date_from):
                    date_from = fields.Datetime.from_string(line.date_from)
                elif date_from > fields.Datetime.from_string(line.date_to):
                    date_from = False

                if date_to > fields.Datetime.from_string(line.date_to):
                    date_to = fields.Datetime.from_string(line.date_to)
                elif date_to < fields.Datetime.from_string(line.date_from):
                    date_to = False

                theo_amt = 0.00
                if date_from and date_to:
                    line_timedelta = fields.Datetime.from_string(line.date_to) - fields.Datetime.from_string(line.date_from)
                    elapsed_timedelta = date_to - date_from
                    if elapsed_timedelta.days > 0:
                        theo_amt = (elapsed_timedelta.total_seconds() / line_timedelta.total_seconds()) * line.planned_amount
            else:
                if line.paid_date:
                    if fields.Datetime.from_string(line.date_to) <= fields.Datetime.from_string(line.paid_date):
                        theo_amt = 0.00
                    else:
                        theo_amt = line.planned_amount
                else:
                    line_timedelta = fields.Datetime.from_string(line.date_to) - fields.Datetime.from_string(line.date_from)
                    elapsed_timedelta = fields.Datetime.from_string(today) - (fields.Datetime.from_string(line.date_from))

                    if elapsed_timedelta.days < 0:
                        # If the budget line has not started yet, theoretical amount should be zero
                        theo_amt = 0.00
                    elif line_timedelta.days > 0 and fields.Datetime.from_string(today) < fields.Datetime.from_string(line.date_to):
                        # If today is between the budget line date_from and date_to
                        theo_amt = (elapsed_timedelta.total_seconds() / line_timedelta.total_seconds()) * line.planned_amount
                    else:
                        theo_amt = line.planned_amount

            line.theoritical_amount = theo_amt